'use strict';
const {
  exec,
  execSync,
} = require('child_process');
const fs = require('fs');
const ospath = require('path');
const { v4: uuidv4 } = require('uuid');
const EXTENSION_NAME = 'antora-maven-extension';

module.exports.register = function({ config }) {
  const {
    mavenPath,
    workingPath,
  } = config;

  const playbookConfig = config;
  const logger = this.require('@antora/logger').get('antora-maven-extension');
  logger.debug({ playbookConfig: playbookConfig }, `${EXTENSION_NAME} playbook config`);

  // validate the playbook config before running the extension for each component, exiting if invalid
  if (!mavenPath) {
    logger.error(`'antora.extension.maven_path' was not provided in the antora playbook. This is required by the ${EXTENSION_NAME} plugin`);
    this.stop(1);
  }
  exec(`${mavenPath} --version`, {}, (error, stdout, stderr) => {
    if (error || stderr) {
      logger.error({
        mavenPath: mavenPath,
        stderr: stderr,
        error: error,
      }, 'invalid \'antora.extension.maven_path\' configured, \'mvn --version\' failed to execute');
      this.stop(1);
    }
    logger.debug({ stdout: stdout }, 'maven appears to be well configured');
  });
  if (!workingPath) {
    logger.error(`'antora.extension.working_path' was not provided in the antora playbook. This is required by the ${EXTENSION_NAME} plugin`);
    this.stop(1);
  }
  // Create a random working directory
  const tempWorkingDir = ospath.join(workingPath, 'antora-maven-extension', uuidv4());
  if (!fs.existsSync(tempWorkingDir)) {
    fs.mkdirSync(tempWorkingDir, { recursive: true });
  }

  // handle event
  this.on('contentAggregated', ({ contentAggregate }) => {
    contentAggregate.forEach(({
      name,
      version,
      files,
      extensions,
    }) => {
      // return early if component:version does not define any antora-maven-extension config
      if (extensions === undefined || extensions.mavenExtension === undefined) {
        logger.debug({
          name: name,
          version: version,
        }, 'has no mavenExtension config, nothing to do here');
        return;
      }

      const componentVersionConfig = extensions.mavenExtension;
      logger.debug({
        name: name,
        version: version,
        playBookConfig: config,
        componentVersionConfig: componentVersionConfig,
      }, 'has mavenExtension config');

      // TODO: validate the componentVersion antora-maven-extension config
      const {
        mavenSymlinkRootDir,
        commands,
        exampleFile, // TODO: use mappings, this is just for testing the rest
        // mappings,
      } = componentVersionConfig;

      // Copy files into the temporary working directory before running maven commands against them
      const componentVersionWorkingDir = ospath.join(tempWorkingDir, name, version);
      logger.debug({
        componentVersionWorkingDir: componentVersionWorkingDir,
        mavenSymlinkRootDir: mavenSymlinkRootDir,
      }, 'Copying symlinked maven root directory into temporary working directory');
      fs.mkdirSync(componentVersionWorkingDir, { recursive: true });
      files
        .filter((file) => file.path.startsWith(mavenSymlinkRootDir))
        .forEach((file) => {
          const toFilePath = ospath.join(componentVersionWorkingDir, file.path);
          const toFileParentDir = ospath.dirname(toFilePath);
          fs.mkdirSync(toFileParentDir, { recursive: true });
          fs.writeFileSync(toFilePath, file.contents.toString('utf-8'), {
            encoding: 'utf8',
            flag: 'w',
          });
        });

      commands.forEach((command) => {
        const stdout = execSync(
          `${mavenPath} -f ${mavenSymlinkRootDir}/pom.xml ${command}`,
          {
            cwd: componentVersionWorkingDir,
            maxBuffer: 10 * 1000 * 1024,
            stdio: 'pipe',
          }
        );

        if (stdout) {
          logger.debug({
            mavenPath: mavenPath,
            mavenRootDir: mavenSymlinkRootDir,
            command: command,
            stdout: stdout.toString().split('\n'),
          }, 'maven command result');
        }
      });

      // put the target file in the componentVersions files
      // TODO
    });
  });
};
